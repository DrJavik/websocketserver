<?php
// Load required files
require(dirname(__DIR__).'/vendor/autoload.php');
require(dirname(__DIR__).'/config/config.inc.php');


// Some namespace stuff
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;

use PhoenixSRV\Chat;
use PhoenixSRV\ConsoleMessaging;
use PhoenixSRV\MySQLDatabase;

$msg = new ConsoleMessaging();
$db  = new MySQLDatabase();

$version = $config["version"]["major"].".".
                   $config["version"]["minor"].".".
                   $config["version"]["patch"];

// Grettings user
$msg->out("Welcome to ".$config["name"]." v$version!");
$msg->out("Listening on ".$config["host"].":".$config["port"]." for new connections.");

    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new Chat()
            )
        ),
        $config["port"]
    );

    $server->run();