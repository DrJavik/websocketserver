<?php

/* 
 * Copyright (C) 2016 Javik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

$config = array("version" => array("major"    => 1,
                                   "minor"    => 0,
                                   "patch"    => 0),
                "name"    => "PhoenixSRV",
                "host"    => "localhost",
                "port"    => 8080,
                "MySQL"   => array("dbuser"   => "phoenixsrv",
                                   "dbpass"   => "phoenixsrv",
                                   "dbname"   => "phoenixsrv",
                                   "dbhost"   => "localhost",
                                   "dbport"   => 3306,
                                   "dbprefix" => "srv_")
);