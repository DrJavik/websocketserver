<?php

/*
 * Copyright (C) 2017 Javik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PhoenixSRV;

/**
 * Description of ConsoleMessaging
 *
 * @author Javik
 */
class ConsoleMessaging {
    
    /**
     * Prints a message to the server console
     * @global array $config The global configuration array defined in the config.inc.php
     * @param string $message The Message to say
     * @param bool $linebreak Use linebreak or not
     */
    function out($message, $linebreak = true) {
        global $config;
        $prefix  = "[".$config["name"]."]";
        
        if($linebreak==true) {
            echo("$prefix $message".PHP_EOL);
        } else {
            echo("$prefix $message");
        }
    }
}
