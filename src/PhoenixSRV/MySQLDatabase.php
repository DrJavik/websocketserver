<?php

/*
 * Copyright (C) 2017 javik
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

namespace PhoenixSRV;

/**
 * Description of MySQLDatabase
 *
 * @author javik
 */
class MySQLDatabase {
    private $mysql, 
            $host, 
            $username, 
            $passwd, 
            $dbname, 
            $port,
            $prefix; 
         
    public function __construct() {
        global $config;
        
        $this->host     = $config["MySQL"]["dbhost"];
        $this->username = $config["MySQL"]["dbuser"];
        $this->passwd   = $config["MySQL"]["dbpass"];
        $this->dbname   = $config["MySQL"]["dbname"];
        $this->port     = $config["MySQL"]["dbport"];
        $this->prefix   = $config["MySQL"]["dbprefix"];
        
        $this->mysql = new \mysqli($this->host, $this->username, $this->passwd, $this->dbname, $this->port);
        
        if ($this->mysql->connect_error) {
            die('Connect Error (' . $this->mysql->connect_errno . ') '
            . $this->mysql->connect_error);
        }
        
    }
    
    /**
     * Connects the bot to a mysql database. (Optional)
     * @param string $host
     * @param string $username
     * @param string $passwd
     * @param string $dbname
     * @param integer $port
     * @param string $prefix
     */
    public function connect($host = NULL,
                            $username = NULL, 
                            $passwd = NULL, 
                            $dbname = NULL, 
                            $port = NULL, 
                            $prefix = NULL) {
        
        /*
         * Checking arguments
         */
        if($host==NULL) {
            $host = $this->host;
        } 
        if($username==NULL) {
            $username = $this->$username;
        }
        if($passwd==NULL) {
            $passwd = $this->passwd;
        }
        if($dbname==NULL) {
            $dbname = $this->dbname;
        }
        if($port==NULL) {
            $port = $this->port;
        }
        if($prefix==NULL) {
            $prefix = $this->prefix;
        }
        /*
         * End
         */
        
        $this->mysql = new \mysqli($host, $username, $passwd, $dbname, $port);
    }
    
    public function disconnect() {
        $this->mysql->close();
    }
}
